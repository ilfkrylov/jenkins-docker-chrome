FROM jenkins/jenkins:2.319.2-lts-jdk8

USER root

RUN apt-get update && apt-get install -y apt-transport-https \
  ca-certificates curl gnupg2 \
  software-properties-common
RUN curl -4fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
RUN apt-get update && apt-get install -y docker-ce-cli

RUN apt-get update  && \
    apt-get install -y python3-pip && \
    python3 -m pip install pytest allure-pytest

RUN apt-get update && apt-get install -y wget && \
    wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list && \
    apt-get update  && \
    apt-get install -y google-chrome-stable xvfb && \
    chown jenkins:jenkins /usr/bin/google-chrome && \
    chmod 755 /usr/bin/google-chrome && \
    chown jenkins:jenkins /usr/bin/google-chrome-stable && \
    chmod 755 /usr/bin/google-chrome-stable

RUN mkdir -p /var/jenkins_home/workspace/drivers && \
    cd /var/jenkins_home/workspace/drivers && \
    wget https://chromedriver.storage.googleapis.com/$(google-chrome --version | awk '{print $3}')/chromedriver_linux64.zip && \
    unzip chromedriver_linux64.zip && \

ENV CHROME_BIN='/usr/bin/google-chrome-stable'

USER jenkins